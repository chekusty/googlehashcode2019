using System;
using System.IO;
using Xunit;

namespace GoogleHashCode2019.Tests
{
    public class SolutionTests
    {
        [Theory]
        [InlineData("..\\..\\..\\Data\\a_example.txt", "..\\..\\..\\..\\Output\\a_example.txt.out")]
        [InlineData("..\\..\\..\\Data\\b_lovely_landscapes.txt", "..\\..\\..\\..\\Output\\b_lovely_landscapes.txt.out")]
        [InlineData("..\\..\\..\\Data\\c_memorable_moments.txt", "..\\..\\..\\..\\Output\\c_memorable_moments.txt.out")]
        [InlineData("..\\..\\..\\Data\\d_pet_pictures.txt", "..\\..\\..\\..\\Output\\d_pet_pictures.txt.out")]
        [InlineData("..\\..\\..\\Data\\e_shiny_selfies.txt", "..\\..\\..\\..\\Output\\e_shiny_selfies.txt.out")]
        public void Run_Solution(string inputFilePath, string outputFilePath)
        {
            Solution s = new Solution();

            var inputFullPath = Path.GetFullPath(inputFilePath);
            var outputFullPath = Path.GetFullPath(outputFilePath);
            s.Run(inputFullPath, outputFullPath);
        }
    }
}
