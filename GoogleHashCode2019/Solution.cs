﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace GoogleHashCode2019
{

    public class Solution
    {
        private int numberOfPhotos = 0;
        private List<Slide> Slides;
        private List<Photo> Photos;
        private Dictionary<int, List<Slide>> GroupedByTagsNumberSlides;

        public void Run(string inDataPath, string outDataPath)
        {
            #region Read data 

            Photos = new List<Photo>();
            Slides = new List<Slide>();

            using (FileStream fs = new FileStream(inDataPath, FileMode.Open))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    numberOfPhotos = Int32.Parse(sr.ReadLine());

                    for (int i = 0; i < numberOfPhotos; i++)
                    {
                        var line1Values = sr.ReadLine().Split(" ");

                        var photo = new Photo
                        {
                            Type = line1Values[0] == "H" ? 0 : 1,
                            Tags = new List<string>(),
                            Index = i
                        };

                        var numberOfTags = int.Parse(line1Values[1]);
                        for (int j = 2; j < line1Values.Length; j++)
                        {
                            photo.Tags.Add(line1Values[j]);
                        }

                        Photos.Add(photo);
                    }
                }
            }
            #endregion Read data 

            //Solution
            Slides = CreateSlides();
            GroupedByTagsNumberSlides = GetSlidesGroupedByTagsNumber();


            var numberOfSlides = Slides.Count;

            var result = new List<Slide>();
            var curSlide = PickTheFirstSlide();
            result.Add(curSlide);
            
            for (int i = 1; i < numberOfSlides; i++)
            {
                var theBestMatch = PickTheBestMatch(curSlide);
                result.Add(theBestMatch);
                curSlide = theBestMatch;
            }

            #region Write data  
            using (FileStream fs = new FileStream(outDataPath, FileMode.OpenOrCreate))
            {
                using (StreamWriter sr = new StreamWriter(fs))
                {
                    sr.WriteLine(result.Count);

                    foreach (var slide in result)
                    {
                        sr.WriteLine(slide.ToString());
                    }
                }
            }
#endregion Write data 
        }
        public Dictionary<int, List<Slide>> GetSlidesGroupedByTagsNumber()
        {
            var dictionary = new Dictionary<int, List<Slide>>();

            foreach (var slide in Slides)
            {
                if (dictionary.TryGetValue(slide.Tags.Count, out List<Slide> slides))
                {
                    slides.Add(slide);
                }
                else
                {
                    dictionary[slide.Tags.Count] = new List<Slide>();
                    dictionary[slide.Tags.Count].Add(slide);
                }
            }

            return dictionary;
        }


        public Slide PickTheFirstSlide()
        {
            var averageNumberOfTags = Slides.Sum(s => s.Tags.Count);
            averageNumberOfTags = averageNumberOfTags / Slides.Count;
            var numberOfSlides = Slides.Count;

            var medianIndex = 0;
            var minDif = Math.Abs(averageNumberOfTags - Slides[medianIndex].Tags.Count);
            for (int i = 0; i < numberOfSlides; i++)
            {
                var curDif = Math.Abs(Slides[i].Tags.Count - averageNumberOfTags);
                if (curDif < minDif)
                {
                    minDif = curDif;
                    medianIndex = i;
                }

                if (minDif == 0) break;
            }

            var result = Slides[medianIndex];

            var list = GroupedByTagsNumberSlides[result.Tags.Count];
            list.Remove(result);
            Slides.Remove(result);
            return result;
        }

        public Slide PickTheBestMatch(Slide slide)
        {
            var perfectMatch = slide.Tags.Count / 2;

            //Pick the group to start search
            var startIndex = slide.Tags.Count;

            //var sortedKeys = GroupedByTagsNumberSlides.Keys.OrderBy(key => Math.Abs(slide.Tags.Count - key)).ToArray();
            var sortedKeys = GroupedByTagsNumberSlides.Keys.ToList();
            Tuple<int, Slide> bestMatch = null;

            for (var i = 0; i < sortedKeys.Count; i++)
            {
                var slides = GroupedByTagsNumberSlides[sortedKeys[i]];
                var bestMatchInGroup = GetTheBestMatchSlideInGroup(slide, slides);
                if (bestMatchInGroup == null) continue;

                bestMatch = bestMatchInGroup;
                break;

                //if (bestMatch == null) bestMatch = bestMatchInGroup;
                //else
                //{
                //    if (bestMatchInGroup.Item1 > bestMatch.Item1) bestMatch = bestMatchInGroup;
                //}

                //if (bestMatch.Item1 == perfectMatch) break;
            }

            var list = GroupedByTagsNumberSlides[bestMatch.Item2.Tags.Count];
            list.Remove(bestMatch.Item2);

            Slides.Remove(bestMatch.Item2);
            return bestMatch.Item2;
        }

        public Tuple<int, Slide> GetTheBestMatchSlideInGroup(Slide slideToTest, List<Slide> slidesForTest)
        {
            if (!slidesForTest.Any()) return null;

            //var maxScore = 0;
            //var maxScoreIndex = -1;

            //for(var i = 0; i < slidesForTest.Count; i++)
            //{
            //    var score = CalculateScoreForSlide(slideToTest, slidesForTest[i]);
            //    if (score >= maxScore)
            //    {
            //        maxScoreIndex = i;
            //        maxScore = score;
            //    }
            //}

            return new Tuple<int, Slide>(CalculateScoreForSlide(slideToTest, slidesForTest[0]), slidesForTest[0]);
        }

        public int CalculateScoreForSlide(Slide slide1, Slide slide2)
        {
            var intersection = slide1.Tags.Intersect(slide2.Tags).Count();

            var temp = new int[] { slide1.Tags.Count - intersection, slide2.Tags.Count - intersection, intersection };

            return temp.Min();
        }


        public List<Slide> CreateSlides()
        {
            var result = new List<Slide>();

            var horizontalPhotos = Photos.Where(p => p.Type == 0).ToList();
            if(horizontalPhotos.Any())
            {
                var averageNumberOfTagsForH = horizontalPhotos.Sum(p => p.Tags.Count);
                averageNumberOfTagsForH = averageNumberOfTagsForH / horizontalPhotos.Count();
            }

            var verticalPhotos = Photos.Where(p => p.Type == 1);
            var orderedVerticalPhotos = verticalPhotos.OrderBy(p => p.Tags.Count).ToList();
            var iterator = new Iterator();

            result = horizontalPhotos.Select(hp => new Slide { Index = iterator.GetNextValue(), hPhoto = hp }).ToList();

            int i = 0;
            int j = orderedVerticalPhotos.Count()-1;

            while (i < j)
            {
                var slide = new Slide
                {
                    Index = iterator.GetNextValue(),
                    vPhoto1 = orderedVerticalPhotos[i],
                    vPhoto2 = orderedVerticalPhotos[j]
                };

                result.Add(slide);

                i++;
                j--;
            }

            return result;
        }
    }

    public class Iterator
    {
        private int i = 0;
        public int GetNextValue()
        {
            return i++;
        }
    }

    public class Photo
    {
        //0 = H, 1 = V
        public int Type;
        public int Index;
        public List<string> Tags;
    }

    public class Slide
    {
        public int Index;

        public Photo hPhoto;
        public Photo vPhoto1;
        public Photo vPhoto2;

        private List<string> _tags;
        public List<string> Tags
        {
            get
            {
                if (_tags == null)
                {
                    _tags = GetTags();
                }

                return _tags;
            }
        }

        private List<string> GetTags()
        {
            if (hPhoto != null)
            {
                return hPhoto.Tags;
            }
            else
            {
                var result = vPhoto1.Tags.ToHashSet();

                foreach (var tag in vPhoto2.Tags)
                {
                    result.Add(tag);
                }

                return result.ToList();
            }
        }

        public override string ToString()
        {
            return hPhoto != null ? $"{hPhoto.Index}" : $"{vPhoto1.Index} {vPhoto2.Index}";
        }
    }

}
